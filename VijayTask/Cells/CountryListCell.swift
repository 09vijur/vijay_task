//
//  CountryListCell.swift
//  VijayTask
//
//  Created by Sunfocus on 29/03/21.
//

import UIKit

class CountryListCell: UITableViewCell {
    
    @IBOutlet weak var lblDialingCode: UILabel!
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var btnSelect: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
