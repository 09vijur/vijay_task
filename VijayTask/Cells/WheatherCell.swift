//
//  WheatherCell.swift
//  VijayTask
//
//  Created by Sunfocus on 29/03/21.
//

import UIKit

class WheatherCell: UITableViewCell {
    
    //MARK:IBOutlets
    @IBOutlet weak var imgCloud:UIImageView!
    @IBOutlet weak var lblTemp:UILabel!
    @IBOutlet weak var lblCityName:UILabel!
    @IBOutlet weak var lblTempStatus:UILabel!
    @IBOutlet weak var lblData:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
