//
//  CountryAndCityPopUpVC.swift
//  VijayTask
//
//  Created by Sunfocus on 29/03/21.
//

import UIKit
import Alamofire

class CountryAndCityPopUpVC: UIViewController {

    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet weak var tfSearchField: UITextField!
    @IBOutlet weak var tableCountry: UITableView!
    @IBOutlet weak var countryTableBottomConstraint: NSLayoutConstraint!
    
    typealias SelectCompletion = (_ cityName: String) -> Void
    typealias AllCitesDataModel = [CountryDataModel]
    
    // MARK: - Variables
    var arrayCity = [CountryDataModel]()
    var arrayFiltered = [CountryDataModel]()
    var countryCodeBlock : SelectCompletion!
        
    //MARK: Object
    var objString = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tfSearchField.delegate = self
        self.loadJson(completion: { (success) in
            self.tableCountry.reloadData()
        })
    }
    
    //MARK: Dismiss button action
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK : Method For fetching Data From JSON file
    func loadJson(completion: @escaping (_ success: Bool)->Void = { _ in }){
        let decoder = JSONDecoder()
        do {
            let url = Bundle.main.url(forResource: "allcites", withExtension: "json")
            let data = try? Data(contentsOf: url!)
            self.arrayCity = try! decoder.decode(AllCitesDataModel.self, from: data!)
            self.arrayCity = self.arrayCity.sorted(by: { $0.fields?.asciiName ?? "" < $1.fields?.asciiName ?? "" })
            self.arrayFiltered = self.arrayCity
            completion(true)
        }catch let error {
            print(error)
            completion(false)
        }
    }
}

// MARK: - UITableView Delegate and DataSource Methods
extension CountryAndCityPopUpVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrayFiltered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell  = tableView.dequeueReusableCell(withIdentifier: "CountryListCell") as! CountryListCell
        let countryData  = arrayFiltered[indexPath.row]
        
        if arrayFiltered[indexPath.row].fields?.asciiName == objString{
            cell.btnSelect.isHidden = false
        }else{
            cell.btnSelect.isHidden = true
        }
        cell.lblCountryName.text = countryData.fields?.asciiName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let countryData  = arrayFiltered[indexPath.row]
        if (self.countryCodeBlock != nil){
            self.countryCodeBlock!(countryData.fields?.asciiName ?? "")
        }
        self.objString = arrayFiltered[indexPath.row].fields?.asciiName ?? ""
        self.tableCountry.reloadData()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 50
    }
    
}

// MARK: - UITextField Delegate  Methods
extension CountryAndCityPopUpVC: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let searchStringLocal = textFieldText.replacingCharacters(in: range, with: string)
        
        if searchStringLocal.count == 0{
            self.arrayFiltered.removeAll()
            self.arrayFiltered = self.arrayCity
            self.tableCountry.reloadData()
        }else{
            arrayFiltered = searchStringLocal.isEmpty ? arrayCity : arrayCity.filter({(objCity: CountryDataModel) -> Bool in
                return objCity.fields?.asciiName?.range(of: searchStringLocal, options: .caseInsensitive) != nil
            })
            self.tableCountry.reloadData()
            
        }
        return true
    }
}

