//
//  ViewController.swift
//  VijayTask
//
//  Created by Sunfocus on 28/03/21.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    
    //MARK: Properties
    @IBOutlet weak var tableWhether: UITableView!
    
    //MARK: Variable
    var statusCode = Int()
    var objWehtherData: WetherDataModel?
    var arayShowData = ["Sunrise:","Sunset:","Humidity:","Temp Max:","Temp Min:","Wind:"]

    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Weather"
        self.setData()
    }
    
    //MARK: Another selection button action
    @IBAction func btnAnotherCityAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: .main).instantiateViewController(identifier: "CountryAndCityPopUpVC") as! CountryAndCityPopUpVC
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle   = .crossDissolve
        vc.countryCodeBlock = { (name) in
            self.openWetherMap(city: name) { (sucess) in
                self.tableWhether.reloadData()
                print(sucess)
            }
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: Method for set data
    func setData(){
        let vc = UIStoryboard.init(name: "Main", bundle: .main).instantiateViewController(identifier: "CountryAndCityPopUpVC") as! CountryAndCityPopUpVC
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle   = .crossDissolve
        vc.countryCodeBlock = { (name) in
            self.openWetherMap(city: name) { (sucess) in
                self.tableWhether.reloadData()
                print(sucess)
            }
        }
        self.present(vc, animated: true, completion: nil)
    }
    
}

//MARK:UITableViewDelegates & DataSource
extension ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            return arayShowData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cloudCell") as! WheatherCell
            cell.lblTemp.text = "Temp: \(((objWehtherData?.main?.temp ?? 0.0).rounded(toPlaces: 2) - 273.15).rounded(toPlaces: 2))°C"
            cell.lblCityName.text = objWehtherData?.name?.capitalized
            cell.lblTempStatus.text = objWehtherData?.weather?[indexPath.row].weatherDescription?.capitalized
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell") as! WheatherCell
            cell.lblData.text = arayShowData[indexPath.row]
            
            let sunRise = Date(timeIntervalSince1970: Double(objWehtherData?.sys?.sunrise ?? 0))
            let sunSet = Date(timeIntervalSince1970: Double(objWehtherData?.sys?.sunset ?? 0))
                        
            switch indexPath.row {
            case 0:
                cell.lblTemp.text = sunRise.getFormattedDate(format: "h:mm a")
                break
            case 1:
                cell.lblTemp.text = sunSet.getFormattedDate(format: "h:mm a")
                break
            case 2:
                cell.lblTemp.text = "\(objWehtherData?.main?.humidity ?? 0)"
                break
            case 3:
                cell.lblTemp.text = "\(((objWehtherData?.main?.tempMax ?? 0.0).rounded(toPlaces: 2) - 273.15).rounded(toPlaces: 2))°C"
                break
            case 4:
                cell.lblTemp.text = "\(((objWehtherData?.main?.tempMin ?? 0.0).rounded(toPlaces: 2) - 273.15).rounded(toPlaces: 2))°C"
                break
            case 5:
                cell.lblTemp.text = "\((objWehtherData?.wind?.speed ?? 0.0).rounded(toPlaces: 2))m/s"
                break
            default:
                break
            }
            return cell
        }
            
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension
        }else{
            return 50.0
        }
    }
}


//MARK: Api call
extension ViewController{
    
    //Mthod for api
    func openWetherMap(city: String, completion: @escaping (_ success: Bool)->Void = { _ in }) {

        let apiKey = "cbf3bb9e62f33896f43fb801a052d1df"
        let state = city
        let countryCode = "IN"
        var param = [String:Any]()
        param["q"] = "\(state),\(countryCode)"
        param["appid"] = apiKey
        let originalString = "https://api.openweathermap.org/data/2.5/weather"
        Alamofire.request(URL(string: originalString)!, method: HTTPMethod.get, parameters: param, headers: nil).responseJSON { (response) in
            do {
                print(response)
                if let headerResponse = response.response {
                    self.statusCode = headerResponse.statusCode
                }
                let responseData = try JSONDecoder().decode(WetherDataModel.self, from: response.data ?? Data())
                if self.statusCode == 200 {
                    self.objWehtherData = responseData
                    completion(true)
                }else{
                    completion(false)
                }
            }catch let error {
                print(error)
            }
        }
    }
}

//MARK: Double Extension
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

//MARK: Date formator extension
extension Date {
   func getFormattedDate(format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: self)
    }
}

